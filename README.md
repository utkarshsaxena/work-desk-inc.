# README #

## Introduction:  

This is a platform for young motivated individuals to offer products and service to an audience looking for young talent with fresh (and hopefully inexpensive) ideas. This platform will be created such that it can easily change with the needs of the customers. 

Click on the following link to view the application: **[Work Desk, Inc.](http://workdeskinc.heroku.com)**

Moving forward, I will build a team on my own to offer this platform to benefit user/students.


## Technologies: ##

* HTML/Swig Templating
* CSS/SASS
* AngularJs
* NodeJs/ExpressJs
* MongoDB
* ReactJS
* Browserify
* Grunt
* Moltin API

## Purpose: ##

* Train to become a full-stack JavaScript developer
* Get out of my comfort zone to include audience into my design process/journey. Work with criticism and feedback. Create a facebook page for this purpose where I post my progress.

## Procedure: ##

1. Browse to **[Work Desk, Inc.](http://workdeskinc.heroku.com)**



## Timestamp: ##

**May, 2015 – August, 2015