var handleProfileInfo = require('../controllers/handleProfileInfo'),
    moltinController = require('../controllers/moltinController');

module.exports = function(express, app, passport, config, mongoose, q, fs, os, formidable, gm) {
    var router = express.Router();
    
    
    // Middlewares section
    // Check if the user is authenticated - No redirect
    function securePages(req, res, next) {
        if(req.isAuthenticated()){
            req.session.userLoggedIn = true;
            // Convert Mongo ID to a string for use in URL params
            if (req.user){
                req.user.uniqueUserID = req.user._id.toString();
            }            
            next();
        } else {
            req.session.userLoggedIn = false;
            next();  
        }
    }
    
     // Redirect to home if logged out
    function restrictedPagesRedirect(req, res, next) {
        if(req.isAuthenticated()){
            req.session.userLoggedIn = true;
            // Convert Mongo ID to a string for use in URL params
            req.user.uniqueUserID = req.user._id.toString();
            next();
        } else {
            req.session.userLoggedIn = false;
            res.redirect('/');
        }
    }
    
    // Redirect to home after logging in
    function securePagesRedirect(req, res, next) {
        if(req.isAuthenticated()){
            req.session.userLoggedIn = true;
            // Convert Mongo ID to a string for use in URL params
            req.user.uniqueUserID = req.user._id.toString();
            res.redirect('/');
        } else {
            req.session.userLoggedIn = false;
            next();
        }
    }
    
    // End Middlewares section
    
    // Homepage Route
    router.get('/', securePages, function(req, res){
        if(req.user){
            var userID = req.user.uniqueUserID;        
        }
       
        res.render('index', {
            userLoggedIn: req.session.userLoggedIn || false,
            user: req.user,
            homeTab: "active",
            userID: userID || null
        });
    });
    
    // Error Page
    router.get('/error', securePages,function(req, res){
        res.render('error', {
            errorMsg: req.session.userLoggedIn,
            userLoggedIn: req.session.userLoggedIn ||false
        });
    });
    
    // Log In Page
    router.get('/login', securePagesRedirect,function(req, res){
        res.render('logInPage', {
            userLoggedIn: req.session.userLoggedIn ||false
        });
    });
    
    // Dashboard Page
    router.get('/dashboard', restrictedPagesRedirect, function(req, res, next){
        var userID = req.user.uniqueUserID;
        // Logic to use mongo to retrieve data pertaining to a specific user goes here
        // Example userModel.findOne({'profileID': profile.id}, function(err, result) {(result) done(null, result);}
        res.render('dashboard', {
            userID: userID,
            userLoggedIn: req.session.userLoggedIn || false,
            user: req.user
        });
    });
    
    router.post('/dashboard', function(req, res) {
        var formData, resObject = {};
        
        formData = req.body;        
        handleProfileInfo(config, mongoose, q).handleProfileFormData(formData).then(function (data) {
            resObject.success = true;
            resObject.message = 'Form successfully submitted.';
            resObject.userInfo = data;
            res.json(resObject);
        }, function (error) {
            resObject.errorInfo = error;
            resObject.success = false;
            resObject.message = 'Data could not be saved';
            res.json(resObject);
        });
    });
    
    // Get User information from database
    // Dashboard Page
    router.post('/getUserInformation', function(req, res, next){
        var userMongoID, resObject = {};
        userMongoID = req.query.userid;
        handleProfileInfo(config, mongoose, q).getUserprofileData(userMongoID).then(function (data) {
            resObject.success = true;
            resObject.message = 'Profile Data Retrieved';
            resObject.userInfo = data;
            res.json(resObject);
        }, function (error) {
            resObject.errorInfo = error;
            resObject.success = false;
            resObject.message = 'Data could not be retrieved.';
            res.json(resObject);
        });        
    });
    
    
    router.post('/createProduct', function(req, res, next) {
        var resObject = {};
        moltinController.createProducts(req.body).then(function (data) {
            resObject.success = true;
            resObject.message = 'Product Created';
            resObject.userInfo = data;
            res.json(resObject);
        }, function (error) {
            resObject.errorInfo = error;
            resObject.success = false;
            resObject.message = 'Failed to create a product.';
            res.json(resObject);
        });  
    });
    
    router.post('/editProduct', function(req, res, next) {
        var resObject = {},
            productid = req.query.productid;
        moltinController.updateProduct(productid, req.body).then(function (data) {
            resObject.success = true;
            resObject.message = 'Product Updated';
            resObject.userInfo = data;
            res.json(resObject);
        }, function (error) {
            resObject.errorInfo = error;
            resObject.success = false;
            resObject.message = 'Failed to update the product.';
            res.json(resObject);
        });  
    });
    
    router.post('/createUserBrand', function(req, res, next) {
        var resObject = {};
        moltinController.createUserBrand(req.body).then(function (data) {
            resObject.success = true;
            resObject.message = 'Brand Created';
            resObject.brandInfo = data;
            res.json(resObject);
        }, function (error) {
            resObject.errorInfo = error;
            resObject.success = false;
            resObject.message = 'Failed to create a user Brand.';
            res.json(resObject);
        });  
    }); 
    
    router.post('/uploadProductImage', function(req, res, next) {
        var resObject = {},
            productId = req.query.productid,
            tmpFile, newFile, fileName;
        
        var newForm = new formidable.IncomingForm();
        newForm.keepExtensions = true;
        newForm.parse(req, function(err, fields, files){
            
            //handle error
            tmpFile = files.upload.path;
            fileName = files.upload.name;
            newFile = os.tmpDir() + '/' + fileName;
        })
        
        newForm.on('end', function(){
            fs.rename(tmpFile, newFile, function(){
                // resize the image and upload this file.
                gm(newFile).resize(400).write(newFile, function(){
                    // Upload to moltin
                    fs.readFile(newFile, function(err, buffer){
                        var data = {
                            file: fs.createReadStream(newFile),
                            name: fileName,
                            assign_to: productId
                        }
                        moltinController.sendproductImages(data).then(function (data) {
                            resObject.success = true;
                            resObject.message = 'Image Uploaded!';
                            resObject.imageInfo = data;
                            res.json(resObject);
                        }, function (error) {
                            resObject.success = false;
                            resObject.message = 'Image failed to upload.';
                            resObject.errorInfo = error;
                            res.json(resObject);
                        });
                    })
                })
            })        
        })
    });
    // Profile Page
    router.get('/profile', restrictedPagesRedirect, function(req, res, next){
        var userData,
            userID = req.user.uniqueUserID,
            profileOwnerUserID = req.query.user;
        
        handleProfileInfo(config, mongoose, q).getUserprofileData(profileOwnerUserID).then(function (data) {
            res.render('profile', {
                profileOwnerUserID: profileOwnerUserID,
                userLoggedIn: req.session.userLoggedIn || false,
                profileOwner: data[0],
                user: req.user,
                userID: userID
            }); 
        }, function (error) {
            resObject.errorInfo = error;
            resObject.success = false;
            resObject.message = 'Data could not be retrieved.';
            res.json(resObject);
        });        
    });
        // Logic to use mongo to retrieve data pertaining to a specific user goes here
        // Example userModel.findOne({'profileID': profile.id}, function(err, result) {(result) done(null, result);}
    
    // Shop Page
    router.get('/shop', securePages, function(req, res){
        res.render('shop', {
            pageName: req.session.userLoggedIn,
            userLoggedIn: req.session.userLoggedIn || false,
            user: req.user,
            shopTab: "active"
        });
    });
    
    // Handpicked Page
    router.get('/handpicked', securePages, function(req, res){
        res.render('handpicked', {
            pageName: req.session.userLoggedIn,
            userLoggedIn: req.session.userLoggedIn || false,
            user: req.user,
            handpickedTab: "active"
        });
    });
    
    // Facebook authentication
    router.get('/auth/facebook', passport.authenticate('facebook'));

    
    router.get('/auth/facebook/callback', passport.authenticate('facebook', {
        successRedirect: '/',
        failureRedirect: '/error'
    }));
    
    // Logout page
    router.get('/logout', function(req, res, next) {
        req.session.userLoggedIn = false;
        req.logout();
        res.redirect('/');        
    });
    
    // Product Page
    router.get('/product', securePages, function(req, res, next){
        var productInfo,
            productImageUrls,
            userID = (req.user) ? req.user.uniqueUserID : false,
            data = {
                slug: req.query.slug                
            };
        
        function filterImageUrls(image){
            var i,
                l = image.length,
                urls = [];
            
            for (i=0; i < l; i++) {
                urls.push(image[i].url.http);
            }            
            return urls;        
        }
        
       moltinController.getFilteredProducts(data).then(function (data) {
           productInfo = JSON.parse(data);
           if(productInfo.result[0].images[0]) {
               productImageUrls = filterImageUrls(productInfo.result[0].images);
               
           }
            res.render('product', {
                userLoggedIn: req.session.userLoggedIn || false,
                user: req.user || false,
                product: productInfo.result[0],
                productImages: productImageUrls,
                userID: userID
            });
        }, function (error) {
            // Render error on the page.
            res.render('product', {
                error: error,
                message: "Woops! Something went wrong. Please try again"
            });
        });
        
    });

    app.use('/', router);
}