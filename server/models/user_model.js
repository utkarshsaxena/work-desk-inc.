var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/config.js');

var workDeskUser = new Schema({
    fullname: String,
    username: String,
    userEmail: String,
    highResPic: String,
    userSchool: Schema.Types.Mixed,
    userDescription: String,
    profileID: String,
    profilePic: String,
    moltinBrandId: Schema.Types.Mixed,
    socialLinks: Schema.Types.Mixed
});

/*workDeskUser.statics.findAndModify = function (query, doc, callback) {
  return this.collection.findAndModify(query, doc, callback);
};*/

module.exports = mongoose.model(config.dbCollection, workDeskUser);     