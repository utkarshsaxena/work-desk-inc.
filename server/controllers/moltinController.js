var moltinController = {},
    q = require('q'),
    request = require('request'),
    authObj = {},
    Moltin = require('moltin').server;

function authenticate() {
    var formData, deferred = q.defer();
    
    formData = {
        grant_type : 'client_credentials',
        client_id : 'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ',
        client_secret: 'KTcczXnLNSTxpwuEcSCeCLadHhYOJUqxEQSV1OMn'
    }
    request.post({
        url:'https://api.molt.in/oauth/access_token',
        formData: formData
    }, function optionalCallback(err, httpResponse, body) {
        if (err) {
            deferred.reject(err);
        }
        deferred.resolve(body);
    });
    
    return deferred.promise;
    
}        

// Create a product
moltinController.createProducts = function(productInfo) {
    var deferred = q.defer(),
        formData = productInfo,
        authObj = {};
    
    // Authenticate with moltin
    authenticate().then(function(data){
        authObj = JSON.parse(data);
        request.post({
            url:'https://api.molt.in/v1/products/',
            formData: formData,
            headers: {
                Authorization : authObj.access_token
            }
        }, function optionalCallback(err, httpResponse, body) {
            if (err || body.status == false) {
                //failed to create a product
                deferred.reject(err);
            }
            deferred.resolve(body);
        });
    }, function(error){
        console.log('Failed to Authenticate');
        console.log(error);
        deferred.reject(error);
    });
    return deferred.promise;
}

// Update a product
moltinController.updateProduct = function(productid, infoToUpdate) {
    var deferred = q.defer(),
        editFormData = infoToUpdate,
        authObj = {};
    // Authenticate with moltin
    authenticate().then(function(data){
        authObj = JSON.parse(data);
        request.put({
            url:'https://api.molt.in/v1/products/' + productid,
            form: editFormData,
            headers: {
                Authorization : authObj.access_token
            }
        }, function optionalCallback(err, httpResponse, body) {
            
            if (err || body.status == false) {
            //failed to create a product
                var error = err || 'Product failed';
                console.log(error);
                deferred.reject(err);
            }
            deferred.resolve(body);
        });
    }, function(error){
        console.log('Failed to Authenticate');
        console.log(error);
        deferred.reject(error);
    });
    return deferred.promise;
}

// Create a user brand
moltinController.createUserBrand = function(brandInfo) {
    var deferred = q.defer(),
        formData = brandInfo,
        authObj = {};
    
    // Authenticate with moltin
    authenticate().then(function(data){
        authObj = JSON.parse(data);
        request.post({
            url:'https://api.molt.in/v1/brands',
            formData: formData,
            headers: {
                Authorization : authObj.access_token
            }
        }, function optionalCallback(err, httpResponse, body) {
            if (err || body.status == false) {
                //failed to create a product
                deferred.reject(err);
            }
            deferred.resolve(body);
        });
    }, function(error){
        console.log('Failed to Authenticate');
        console.log(error);
        deferred.reject(error);
    });
    
    return deferred.promise;
}

// Upload product images

moltinController.sendproductImages = function(formData){
    var deferred = q.defer(),
        authObj = {};
    
    // Authenticate with moltin
    authenticate().then(function(data){
        authObj = JSON.parse(data);
        request.post({
            url:'https://api.molt.in/v1/files',
            formData: formData,
            headers: {
                Authorization : authObj.access_token
            }
        }, function optionalCallback(err, httpResponse, body) {
            var moltinImgData;
            if (err || body.status == false) {
                //failed to create a product
                deferred.reject(err);
            }
            moltinImgData = body;
            deferred.resolve(moltinImgData);
        });
    }, function(error){
        console.log('Failed to Authenticate');
        console.log(error);
        deferred.reject(error);
    });
    
    return deferred.promise;
}

// GET filtered products from moltin (to be edited)
moltinController.getFilteredProducts = function(filterParams) {
    var deferred = q.defer(),
        authObj = {};
    
    // Authenticate with moltin
    authenticate().then(function(data){
        authObj = JSON.parse(data);
        request.get({
            url:'https://api.molt.in/v1/products?slug=' + filterParams.slug,
            headers: {
                Authorization : authObj.access_token
            }
        }, function optionalCallback(err, httpResponse, body) {
            if (err || body.status == false) {
                //failed to create a product
                deferred.reject(err);
            }
            deferred.resolve(body);
        });
    }, function(error){
        console.log('Failed to Authenticate');
        console.log(error);
        deferred.reject(error);
    });
    
    return deferred.promise;
}

module.exports = moltinController;

