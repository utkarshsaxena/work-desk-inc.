
module.exports = function(config, mongoose, q){
    
    var module = {},
        UserModel = mongoose.model(config.dbCollection);
    //define static on model to retrieve information.
    
    
    // handle create/update values of userProfile document
    module.handleProfileFormData = function(formData, deferred) {
        var deferred = q.defer();
        if (formData.brandInfoToMongo) {
            UserModel.findByIdAndUpdate(
                {
                    _id : formData.mongoUserID
                },
                { 
                    $set:{
                        moltinBrandId: {
                            brandID : formData.brandInfoToMongo.brandID ,
                            slug : formData.brandInfoToMongo.slug,
                            title : formData.brandInfoToMongo.title,
                            description : formData.brandInfoToMongo.description
                        }
                    }
                },
                {
                    new : true
                },
                function(err, data) {
                    if (err) {
                        console.error(err);
                        deferred.reject(err);
                    }
                    
                    if(data) {
                        deferred.resolve(data);
                    }
                });
        } else {
            UserModel.findByIdAndUpdate( 
                {
                    _id : formData.mongoUserID
                },
                { 
                    $set:{
                        username: formData.username,
                        userEmail: formData.userEmail,
                        userSchool: {
                            schoolId: formData.userSchool.schoolId,
                            schoolName: formData.userSchool.schoolName,
                            shortName: formData.userSchool.shortName
                        },
                        userDescription: formData.userDescription,
                        socialLinks : {
                            linkedin: formData.socialLinks.linkedin,
                            behance: formData.socialLinks.behance,
                            portfolio: formData.socialLinks.portfolio
                        }
                    }
                },
                {
                    new : true
                },
                function(err, data) {
                    if (err) {
                        console.error(err);
                        deferred.reject(err);
                    }
                    if(data) {
                        deferred.resolve(data);
                    }
                });
        }
        return deferred.promise;
    }
    
    // Get user's profile data from database
    module.getUserprofileData = function(userMongoID) {
        var deferred = q.defer();
        
        UserModel.find( { _id : userMongoID }, function(err, data) {
            if (err) {
                console.error(err);
                deferred.reject(err);
            }
            
            if(data) {
                deferred.resolve(data);                
            }
        });        
        return deferred.promise;
    }
    return module;
}
