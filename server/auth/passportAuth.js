module.exports = function(passport, FacebookStrategy, config, mongoose) {
    
    var UserModel = require('../models/user_model');
    
    // Used for sessions
    passport.serializeUser(function(user, done){
        done(null, user.id);
    });
    
    passport.deserializeUser(function(id, done){
        UserModel.findById(id, function(err, user){
            if(err) console.error(err);
            done(err, user);        
        })
    });
    // passport authentication
    passport.use(new FacebookStrategy({
        clientID: config.fb.appID,
        clientSecret: config.fb.appSecret,
        callbackURL: config.fb.callbackURL,
        profileFields: ['id', 'displayName', 'photos', 'email']
        
    }, function(accessToken, refreshToken, profile, done){
        
        // Check if the user exists in our MongoDB database
        // if not, create one and return the profile
        // if the user exists, simply return the profile
        
        UserModel.findOne({'profileID':profile.id}, function(err, result){
            if(err) {
                console.log('Error at UserModel.findOne');
                console.error(err);                
            };
            
            if(result){                
                done(null, result);   
                
            } else {
                // Create a new user in our Mongo Account if email exists
                var newWorkDeskUser = new UserModel({
                    profileID: profile.id,
                    fullname: profile.displayName,
                    profilePic: profile.photos[0].value || '',
                    userEmail: profile.email || '',
                    highResPic: "https://graph.facebook.com/v2.4/" + profile.id + "/picture" + "?width=400&height=400" + "&access_token=" + accessToken
                });

                newWorkDeskUser.save(function(){
                    done(null, newWorkDeskUser);
                }) 
            }
        });    
    }))
}