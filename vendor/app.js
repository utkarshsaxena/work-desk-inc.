// Main node module

var express = require('express'),
    app = express(),
    cons = require('consolidate'), // Templating library adapter for Express
    MongoClient = require('mongodb').MongoClient, // Driver for connecting to MongoDB
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    path =  require('path'), // For handling and transforming file path
    config = require('./../config/config.js'),
    session = require('express-session'),
    ConnectMongo = require('connect-mongo')(session),
    mongoose = require('mongoose'),
    swig = require('swig'),
    passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy,
    q = require('q'),
    ConnectMongo = require('connect-mongo')(session),
    fs = require('fs'),
    os = require('os'),
    formidable = require('formidable'),
    gm = require('gm');


// Resolve path to access client/server files
var client_files = path.resolve(__dirname, '../minclient/'),
    server_files = path.resolve(__dirname, '../server/');

// Get environment variable
var env = process.env.NODE_ENV || 'development';

console.log(process.env.NODE_ENV + ' environment');

if(env === 'development') {
    // Express middleware to populate 'req.cookies' so we can access cookies
    app.use(cookieParser());
    app.use(session({
        secret: config.sessionSecret,
        saveUninitialized: true,
        resave: true,
        cookie: {}
    }));
} else {
    // Express middleware to populate 'req.cookies' so we can access cookies
    app.use(cookieParser());
    app.use(session({
        secret: config.sessionSecret,
        saveUninitialized: true,
        resave: true,
        store: new ConnectMongo({
            url: config.dbURL,
            stringify: true        
        })    
    }));
    console.log('prod.');
    // Prod Specific settings
}


mongoose.connect(config.dbURL, function(err, db) {
    "use strict";
    if(err) {
        console.log('Error at mongoose.connect');
        console.error(err);
    };
    
    if(db) console.log('db object found');
    
    // Register our templating engine
    app.engine('html', cons.swig);
    app.set('view engine', 'html');
    app.set('views', server_files + '/views');
    
    // Express middleware to serve static files
    app.use(express.static('minclient'));
    
    // Express middleware to populate 'req.body' so we can access POST variables
    // bodyParser() has been depricated, need to call methods separately.
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    
    // Require passport authentication
    app.use(passport.initialize());
    app.use(passport.session());    
    require(server_files + '/auth/passportAuth.js')(passport, FacebookStrategy, config, mongoose);
    
    // Require router file
    require(server_files + '/routes/routes.js')(express, app, passport, config, mongoose, q, fs, os, formidable, gm);    
    
    app.set('port', (process.env.PORT || 5000));
    
    app.listen(app.get('port'), function() {
        console.log('Node app is running on port',
                    app.get('port'));
    });  
});
 