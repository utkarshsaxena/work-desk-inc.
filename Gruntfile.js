module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
		// Project configuration.
		grunt.initConfig({
				karma: {
						unit: {
								configFile: 'karma.conf.js',
								background: false
						}
				},
				casperjs: {
						options: {
								async: {
										parallel: false
								}
						},
						files: ['tests/casper_tests/*.js']
				},
        shell: {
            browserify : {
                command: 'browserify ./minclient/mainScript.min.js |uglifyjs -o ./minclient/mainBrowserScript.min.js -d'
            },
            react: {
                command: 'browserify ./minclient/reactComponentsJS/app.min.js |uglifyjs -o ./minclient/reactComponentsJS/appBrowser.min.js -d'
            },
            reactProfileView: {
                command: 'browserify ./minclient/reactComponentsJS-profileView/app.min.js |uglifyjs -o ./minclient/reactComponentsJS-profileView/appBrowser.min.js -d'
            },
            angular: {
                command: 'browserify ./minclient/dashboardApp/app.min.js |uglifyjs -o ./minclient/dashboardApp/appDashboardBrowser.min.js -d'
            }
        },
        uglify: {
             options: {
                 mangle: false,
                 sourceMap: true,
                 beautify: false
             },
            min: {
                files: grunt.file.expandMapping(['client/*.js', 'client/dashboardApp/*.js', 'client/dashboardApp/controllers/*.js', 'client/reactComponentsJS/*.js', 'client/reactComponentsJS-profileView/*.js'], '', {
                    rename: function(destBase, destPath) {
                        return destBase+'min'+destPath.replace('.js', '.min.js');
                    }
                })
            }
        },
        watch: {
            scripts: {
                files: ['./client/**/*.js','./client/*.js','./minclient/mainScript.min.js', './minclient/reactComponentsJS/app.min'],
                tasks: ['uglify', 'shell:reactProfileView'],
                options: {
                    spawn: false,
                }
            }
        }
    });
    
    // Load the plugin to run karma and casper tests
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-casperjs');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    //grunt.loadNpmTasks('load-grunt-tasks')(grunt);
    
    
    // Default task(s).
    //grunt.registerTask('default', ['uglify']);

};