var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            priceValue: ''            
        }    
    },
    handleChange: function(event){
            console.log(event.target.value);
    },
    render: function() {
        return React.createElement("select", {className: "form-control", onChange: this.handleChange}, 
                    React.createElement("option", {disabled: true, selected: true}, this.props.templateText[5].selectPrice), 
                    React.createElement("option", {"data-price-low": "0", "data-price-high": "20"}, "0 - 20"), 
                    React.createElement("option", {"data-price-low": "20", "data-price-high": "40"}, "20 - 40"), 
                    React.createElement("option", {"data-price-low": "40", "data-price-high": "60"}, "40 - 60"), 
                    React.createElement("option", {"data-price-low": "60", "data-price-high": "9999"}, "60 +")
                )
    }
});


