var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            priceValue: 'low'            
        }    
    },
    handleChange: function(event){
        this.setState({priceValue: event.target.value});
    },
    render: function() {
        this.props.handlePriceInput(this.state.priceValue);
        return React.createElement("select", {
        className: "form-control", 
        onChange: this.handleChange, 
        value: this.state.priceValue}, 
        React.createElement("option", {value: "low"}, "Low to High"), 
        React.createElement("option", {value: "high"}, "High to Low")
                )
    } 
});


