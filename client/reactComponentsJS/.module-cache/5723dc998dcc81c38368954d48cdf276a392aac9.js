var React = require('react');

module.exports = React.createClass({displayName: "exports", 
    render: function() {
        return React.createElement("div", {className: "col-sm-6 col-md-4"}, 
                    React.createElement("div", {className: "thumbnail"}, 
                      React.createElement("img", {src: "#", alt: "#"}), 
                      React.createElement("div", {className: "caption"}, 
                        React.createElement("h3", null, "Thumbnail label"), 
                        React.createElement("p", null, "..."), 
                        React.createElement("p", null, React.createElement("a", {href: "#", className: "btn btn-primary", role: "button"}, "Button"), 
                            React.createElement("a", {href: "#", className: "btn btn-default", role: "button"}, "Button")
                        )
                      )
                    )
                  )
    }
});