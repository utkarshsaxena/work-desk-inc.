var React = require('react');

module.exports = React.createClass({displayName: "exports", 
    render: function() {
        return React.createElement("select", {className: "form-control"}, 
                    React.createElement("option", {"data-price-low": "0", "data-price-high": "9999"}, "Select Price"), 
                    React.createElement("option", {"data-price-low": "0", "data-price-high": "20"}, "0 - 20"), 
                    React.createElement("option", {"data-price-low": "20", "data-price-high": "40"}, "20 - 40"), 
                    React.createElement("option", {"data-price-low": "40", "data-price-high": "60"}, "40 - 60"), 
                    React.createElement("option", {"data-price-low": "60", "data-price-high": "9999"}, "60 +")
                )
    }
});


