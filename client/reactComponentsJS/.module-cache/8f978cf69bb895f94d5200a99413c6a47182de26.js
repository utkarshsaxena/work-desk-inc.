var React = require('react'),
    ProductList = ('./productList.min');

module.exports = React.createClass({displayName: "exports",
    render: function(){
        return React.createElement("section", {className: "products-section"}, 
                        React.createElement("div", {className: "row"}, 
                          React.createElement(ProductList, null)
                        )
                    )    
    }
});