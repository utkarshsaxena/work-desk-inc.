var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            searchInputVal: ''            
        }    
    },
    handleChange: function(event){
        this.setState({searchInputValue: event.target.value});
    },
    render: function() {
        return React.createElement("div", {className: "input-group"}, 
                    React.createElement("input", {
                    type: "text", 
                    className: "form-control", 
                    onChange: this.handleChange, 
                    value: this.state.searchInputVal}), 
                    React.createElement("span", {className: "input-group-btn"}, 
                        React.createElement("button", {className: "btn btn-default", type: "button"}, this.props.templateText[0].go)
                    )
                )
    }
});