var React = require('react');

module.exports = React.createClass({displayName: "exports", 
    handleChange: function(){
        console.log('handleChange');
    
    },
    render: function() {
        return React.createElement("div", {className: "input-group"}, 
                    React.createElement("input", {
                    type: "text", 
                    className: "form-control", 
                    placeholder: this.props.templateText[2].searchFor, 
                    onChange: this.handleChange}), 
                    React.createElement("span", {className: "input-group-btn"}, 
                        React.createElement("button", {className: "btn btn-default", type: "button"}, this.props.templateText[0].go)
                    )
                )
    }
});