var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            priceValue: 'low'            
        }    
    },
    handleChange: function(event){
            console.log(event.target.value);
    },
    render: function() {
        return React.createElement("select", {className: "form-control", onChange: this.handleChange, value: this.state.priceValue}, 
                    React.createElement("option", {disabled: true, selected: true}, this.props.templateText[5].selectPrice), 
                    React.createElement("option", null, "Low to High"), 
                    React.createElement("option", null, "High to Low")
                )
    } 
});


