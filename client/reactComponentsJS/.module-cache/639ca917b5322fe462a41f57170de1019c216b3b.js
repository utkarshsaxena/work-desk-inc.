var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min'); 

var App = React.createClass({displayName: "App",
    render: function() {
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-sm-4 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, null)
                ), 
                React.createElement("div", {className: "col-sm-8 col-md-9 col-lg-10"}, 
                    React.createElement(Products, null)
                )
            )
        )
  }
});

var PRODUCTS = [
    { searchBtn : 'Go!'},
    { dollar: '$'},
    { searchInputPlaceholder: 'Search for...'},
    {},
    {},
    {},
    {}
];

React.render(React.createElement(App, {templateText: PRODUCTS}), document.querySelector('.primary-website-section'));

