var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            searchValue: ''            
        }    
    },
    handleChange: function(event){
        this.setState({searchValue: event.target.value});
    },
    render: function() {
        this.props.handleSearchInput(this.state.searchValue);
        return React.createElement("div", {className: "input-group search-input-wrapper"}, 
                    React.createElement("input", {
                    type: "text", 
                    className: "form-control", 
                    placeholder: this.props.templateText[2].searchFor, 
                    onChange: this.handleChange, 
                    value: this.state.searchValue}
                        )
                )
    }
});