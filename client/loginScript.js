var elements = {
    banner: '.primary-banner',
    jumbotron: '.primary-banner .jumbotron',
    closeBanner: '.hide-banner'
};

jQuery(elements.jumbotron).css('background-image', 'url("/assets/images/login-header-image.jpg")');

jQuery(elements.closeBanner).on('click', function(){
    jQuery(elements.banner).fadeOut();
});