// Script for index.html
var elements = {
    banner: '.primary-banner',
    jumbotron: '.primary-banner .jumbotron',
    closeBanner: '.hide-banner'
};

jQuery(elements.jumbotron).css('background-image', 'url("/assets/images/banner2.jpg")');

jQuery(elements.closeBanner).on('click', function(){
    jQuery(elements.banner).fadeOut();
});