var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            categoryVal: 'any',
            categoryID: null
        }    
    },
    handleChange: function(event){
        console.log(event.target.data-id);
        this.setState({
            categoryVal: event.target.value,
            categoryID: event.target.dataId
        });
    },
    render: function() {
        this.props.handleCategoryInput(this.state.categoryID);
        
        var options = this.props.categories.map(function(categoryDataProps, i){
            return React.createElement("option", {value: categoryDataProps.title, "data-id": categoryDataProps.id, key: i}, categoryDataProps.title)
        });
        
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {
                    className: "form-control", 
                    onChange: this.handleChange, 
                    value: this.state.categoryVal}, 
                        React.createElement("option", null, this.props.templateText[3].allCategories), 
                    options
                    )
                )
    }
});