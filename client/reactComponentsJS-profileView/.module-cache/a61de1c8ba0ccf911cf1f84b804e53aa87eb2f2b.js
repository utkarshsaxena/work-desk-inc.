var React = require('react'),
    ProductList = require('./productList.min');

module.exports = React.createClass({displayName: "exports",
    
    render: function(){
        var productList = React.createElement("div", null) /*this.props.products.productsData.map(function(productDataProps, i){
            return <ProductList {...productDataProps} key={i}/>
        });*/ // Check if I need to use bind       
        return React.createElement("section", {className: "products-section"}, 
                        React.createElement("div", {className: "row"}, 
                            productList
                        )
                    )    
    }
});