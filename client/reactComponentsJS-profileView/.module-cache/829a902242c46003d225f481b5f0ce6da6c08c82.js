var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            categoryVal: 'All Categories'
        }    
    },
    componentDidMount: function() {
        //this.props.handleCategoryInput(this.state.categoryVal);
    },
    handleChange: function(event){
        /*this.setState({
            categoryVal: event.target.value
        });*/
        console.log( event.target.value); // Test if I need a value on the select box or does it pick up from option's value
       // this.props.handleCategoryInput(this.state.categoryVal);
    },
    render: function() {
        
        var options = this.props.categories.map(function(categoryDataProps, i){
            return React.createElement("option", {
            value: categoryDataProps.id, 
            key: i}, 
                categoryDataProps.title
            )
        });
        
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {
                    className: "form-control", 
                    onChange: this.handleChange
                    }, 
                        React.createElement("option", null, this.props.templateText[3].allCategories), 
                    options
                    )
                )
    }
});