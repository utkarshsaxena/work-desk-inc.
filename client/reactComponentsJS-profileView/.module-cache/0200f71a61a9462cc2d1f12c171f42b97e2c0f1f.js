var React = require('react'),
    SearchField = require('./searchField.min'),
    SearchDropDownCategory = require('./searchDropDownCategory.min'),
    SearchDropDownPrice = require('./searchDropDownPrice.min'); 

module.exports = React.createClass({displayName: "exports",
    render: function(){
        return React.createElement("section", {className: "search-module well"}, 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-md-12"}, 
                                React.createElement(SearchField, null), 
                                React.createElement(SearchDropDownCategory, null), 
                                React.createElement("div", {className: "input-group price-dropdown"}, 
                                    React.createElement("div", {className: "input-group-addon"}, "$")
                                    
                                )
                            )
                        ), " "/* .row */
                    )
    }
});