var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min'),
    MoltinFactory = require('./moltinFactory.min');

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            productsList: [],
            categoriesList: []
        }
    },
    componentDidMount: function(){        
        var self = this;
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {
            // Get all products
            self.moltin.Product.List(null, function(products) {
                self.setState({ productsList: products });
            }, function(error) {
                console.error(error);
            });
            
            // Get all categories
            self.moltin.Category.List(null, function(categories) {
                self.setState({ categoriesList: categories }); 
            }, function(error) {
                console.error(error);
            });
        });           
    },
    handleProductState: function(products) {
        this.setState({ productsList: products });
    },
    handleCategoriesState: function(categories){
        this.setState({ categoriesList: categories });    
    },
    handleCategoryChange: function(categoryVal){
        var categoryValue, productList, self=this;
        if(categoryVal === "All Categories") {
            categoryValue =  null;            
        } else {
            categoryValue = { category: categoryVal  };
        }
        
        MoltinFactory(categoryValue)
          .done(function (products) {
            productList = products;
            self.setState({ productsList: products });
        }).fail(function () {
            console.error("Failed to update product list");
        });
    },
    render: function() {        
        return React.createElement("div", {className: "container-fluid"}, 
                    React.createElement("section", {className: "secondary-navbar navbar-static-top"}, 
                        React.createElement(SearchForm, {
                        templateText: this.props.templateText, 
                        categories: this.state.categoriesList, 
                        handleCategoryChange: this.handleCategoryChange})
                ), 
                React.createElement("div", {className: "row"}, 
                    React.createElement("div", {className: "col-xs-12 col-md-12 col-lg-12"}, 
                        React.createElement(Products, {
                        templateText: this.props.templateText, 
                        products: this.state.productsList})
                    )
                )
            )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(React.createElement(App, {templateText: templateText}), document.querySelector('.primary-website-section'));