var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min'); 

window.Moltin = require('../assets/scripts/moltin_1_0_0');

console.log(Moltin);

var App = React.createClass({displayName: "App",
    render: function() {
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-10 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {templateText: this.props.templateText})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {templateText: this.props.templateText, products: this.props.products})
                )
            )
        )
  }
});


var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

var sampleData = {
    productData: [
        {
            product: 'Matte Blackdsfsdfsdfdsfsdf Sunglasses',
            owner: 'UtBooty',
            imageURl: '/assets/images/product1.jpg',
            price: '160',
            rating: '4.0'
        },
        {
            product: 'Brown Rugged Duffle Bag',
            owner: 'Iksovji',
            imageURl: '/assets/images/productHandbag.jpg',
            price: '45',
            rating: '3.5'
        },
        {
            product: 'LED Solar Lantern',
            owner: 'Coleman',
            imageURl: '/assets/images/product2.jpg',
            price: '45',
            rating: '4.5'
        },
        {
            product: 'Brown Rugged Duffle Bag',
            owner: 'Iksovji',
            imageURl: '/assets/images/productHandbag.jpg',
            price: '45',
            rating: '2.2'
        },
        {
            product: 'LED Solar Lantern',
            owner: 'Coleman',
            imageURl: '/assets/images/product2.jpg',
            price: '45',
            rating: '3.0'
        },
        {
            product: 'Matte Black Sunglasses',
            owner: 'UtBooty',
            imageURl: '/assets/images/product1.jpg',
            price: '160',
            rating: '1.5'
        }
    ]
};
    
React.render(React.createElement(App, {templateText: templateText, products: sampleData}), document.querySelector('.primary-website-section'));

