var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            priceValue: ''            
        }    
    },
    handleChange: function(event){
            console.log(event.target.data-value);
    },
    render: function() {
        return React.createElement("select", {className: "form-control", onChange: this.handleChange}, 
                    React.createElement("option", {disabled: true, selected: true}, this.props.templateText[5].selectPrice), 
                    React.createElement("option", {"data-value": "low"}, "Low to High"), 
                    React.createElement("option", {"data-value": "high"}, "High to Low")
                )
    }
});


