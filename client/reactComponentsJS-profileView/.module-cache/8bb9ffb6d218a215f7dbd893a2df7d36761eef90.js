var React = require('react');


module.exports = React.createClass({displayName: "exports",
    render: function() {
        return React.createElement("div", {className: "col-sm-6 col-md-4"}, 
                    React.createElement("div", {className: "thumbnail product-thumbnail", 
                        onMouseEnter: this.onMouseEnterHandler, 
                        onMouseLeave: this.onMouseLeaveHandler}, 
                        React.createElement("div", {className: "thumbnail-footer"}, 
                        React.createElement("p", null, "$", this.props.price)
                    ), 
                      React.createElement("img", {width: "250px", height: "250px", src: this.props.imageURl}), 
                      React.createElement("div", {className: "overlay-text"}, 
                        React.createElement("i", {"data-toggle": "tooltip", title: "Add to Cart!", className: "fa fa-cart-plus fa-2x"}), 
                        React.createElement("h3", {className: "overlay-title"}, this.props.product), 
                        React.createElement("p", {className: "overlay-owner"}, "By ", this.props.owner)
                        
                      )
                    )
                  )
    }
});