var React = require('react'),
    SearchField = require('./searchField.min'),
    SearchDropDownCategory = require('./searchDropDownCategory.min');
    //SearchDropDownPrice = require('./searchDropDownPrice.min');

module.exports = React.createClass({displayName: "exports",
    handleCategoryInput: function(categoryVal){
        // send categroy to the main app for rendering
        this.props.handleCategoryChange(categoryVal);
    },
    render: function(){
        return React.createElement("section", {className: "search-module"}, 
                    React.createElement("form", {className: "form-inline"}, 
                                /* Comment for now <SearchField
                            templateText={this.props.templateText}
                            handleSearchInput={this.handleSearchInput}/> */
                            React.createElement(SearchDropDownCategory, {
                            templateText: this.props.templateText, 
                            handleCategoryInput: this.handleCategoryInput, 
                            categories: this.props.categories})
                                
                            /*<div className="input-group price-dropdown">
                                    <div className="input-group-addon">{this.props.templateText[1].dollar}</div>
                                    
                                    <SearchDropDownPrice
                                    handlePriceInput={this.handlePriceInput}
                                    templateText={this.props.templateText}/>
                                </div> */
                                    /*<button
                                className="btn btn-default"
                                type="button"
                                onClick={this.handleClick}>{this.props.templateText[0].search}</button> */
        )
                    )
    }
});