var React = require('react');

module.exports = React.createClass({displayName: "exports",
    render: function(){
        return React.createElement("section", {className: "products-section"}, 
                        React.createElement("div", {class: "row"}, 
                          React.createElement("div", {class: "col-sm-6 col-md-4"}, 
                            React.createElement("div", {class: "thumbnail"}, 
                              React.createElement("img", {src: "...", alt: "..."}), 
                              React.createElement("div", {class: "caption"}, 
                                React.createElement("h3", null, "Thumbnail label"), 
                                React.createElement("p", null, "..."), 
                                React.createElement("p", null, React.createElement("a", {href: "#", class: "btn btn-primary", role: "button"}, "Button"), 
                                    React.createElement("a", {href: "#", class: "btn btn-default", role: "button"}, "Button")
                                )
                              )
                            )
                          )
                        )
                    )    
    }

});