var React = require('react');

module.exports = React.createClass({displayName: "exports",
    handleChange: function(event){
        this.props.searchValue(event.target.value);    
    },
    render: function() {
        return React.createElement("div", {className: "input-group"}, 
                    React.createElement("input", {
                    type: "text", 
                    className: "form-control", 
                    placeholder: this.props.templateText[2].searchFor, 
                    onChange: this.handleChange, 
                    value: this.props.searchValue}), 
                    React.createElement("span", {className: "input-group-btn"}, 
                        React.createElement("button", {className: "btn btn-default", type: "button"}, this.props.templateText[0].go)
                    )
                )
    }
});