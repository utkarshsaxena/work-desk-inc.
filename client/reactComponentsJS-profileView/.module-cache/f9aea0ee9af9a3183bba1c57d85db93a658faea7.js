module.exports = function(categoryValue) {
    var moltin, productList, deferredObject = jQuery.Deferred();
    
    function filterProductList(brand, productList) {
        var i, id,
            l = productList.length,
            filteredProducts = [];

        for (i = 0; i < l; i++) { 
            if(productList[i].brand && productList[i].brand.data && productList[i].brand.data.id === brand){
                filteredProducts.push(productList[i]);
            }        
        } 
        return filteredProducts;
    }
    moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        moltin.Authenticate(function(data,error) {
            moltin.Product.List(categoryValue, function(products) {
                productList = products;
                deferredObject.resolve(products);
            }, function(error) {
                console.error(error);
                deferredObject.reject(error);                
            });
        });
    return deferredObject;
}