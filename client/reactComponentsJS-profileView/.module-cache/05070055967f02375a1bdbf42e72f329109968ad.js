var React = require('react');

module.exports = React.createClass({displayName: "exports",
    render: function(){
        return React.createElement("section", {className: "search-module well"}, 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-md-12"}, 
                                React.createElement("div", {className: "input-group"}, 
                                    React.createElement("input", {type: "text", className: "form-control", placeholder: "Search for..."}), 
                                    React.createElement("span", {className: "input-group-btn"}, 
                                        React.createElement("button", {className: "btn btn-default", type: "button"}, "Go!")
                                    )
                                ), " ", /* .input-group */

                                React.createElement("div", {className: "dropdown category-dropdown"}, 
                                    React.createElement("select", {className: "form-control"}, 
                                        React.createElement("option", {"data-categoryid": ""}, "Select a Category"), 
                                        React.createElement("option", null, "Apple")
                                    )
                                ), 
                                 React.createElement("div", {className: "input-group price-dropdown"}, 
                                    React.createElement("div", {className: "input-group-addon"}, "$"), 
                                    React.createElement("select", {className: "form-control"}, 
                                        React.createElement("option", {"data-price-low": "0", "data-price-high": "9999"}, "Select Price"), 
                                        React.createElement("option", {"data-price-low": "0", "data-price-high": "20"}, "0 - 20"), 
                                        React.createElement("option", {"data-price-low": "20", "data-price-high": "40"}, "20 - 40"), 
                                        React.createElement("option", {"data-price-low": "40", "data-price-high": "60"}, "40 - 60"), 
                                        React.createElement("option", {"data-price-low": "60", "data-price-high": "9999"}, "60 +")
                                    )
                                )
                            ), " "/* .col-lg-6 */
                        ), " "/* .row */
                    )
    
    }

});