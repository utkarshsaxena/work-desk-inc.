var React = require('react'),
    ProductList = require('./productList.min');

module.exports = React.createClass({displayName: "exports",
    
    render: function(){
        console.log(this.props.products);
        var productList = React.createElement("div", null)
        /*this.props.products.map(function(productDataProps, i){
            return <ProductList {...productDataProps} key={i}/>
        });    */   
        return React.createElement("section", {className: "products-section"}, 
                        React.createElement("div", {className: "row"}, 
                            productList
                        )
                    )    
    }
});