var React = require('react'),
    ProductList = require('./productList.min');

module.exports = React.createClass({displayName: "exports",
    
    render: function(){        
        
        var productList = this.props.products.productDaata.map(function(productDataProps, i){
            return React.createElement(ProductList, React.__spread({},  productDataProps, {key: i}))
        });        
        return React.createElement("section", {className: "products-section"}, 
                        React.createElement("div", {className: "row"}, 
                            productList
                        )
                    )    
    }
});