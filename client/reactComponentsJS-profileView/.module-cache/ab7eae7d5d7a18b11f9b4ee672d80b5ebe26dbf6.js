var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            priceValue: 'low'            
        }    
    },
    handleChange: function(event){
        this.setState({priceValue: event.target.value});
    },
    render: function() {
        console.log(this.state.priceValue);
        return React.createElement("select", {
        className: "form-control", 
        onChange: this.handleChange}, 
                    React.createElement("option", {
        disabled: true
         }, this.props.templateText[5].selectPrice), 
                    React.createElement("option", {value: "low"}, "Low to High"), 
                    React.createElement("option", {value: "high"}, "High to Low")
                )
    } 
});


