var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            categoryVal: 'any'            
        }    
    },
    handleChange: function(event){
        this.setState({categoryVal: event.target.value});
    },
    render: function() {
        this.props.handleCategoryInput(this.state.categoryVal);
        console.log(this.props.categories);
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {
                    className: "form-control", 
                    onChange: this.handleChange, 
                    value: this.state.categoryVal}, 
                        React.createElement("option", null, this.props.templateText[3].allCategories), 
                        React.createElement("option", {value: "cat1"}, this.props.templateText[4].categoryOptions)
                    )
                )
    }
});