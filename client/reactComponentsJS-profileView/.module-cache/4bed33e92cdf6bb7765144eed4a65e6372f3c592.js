var React = require('react');

module.exports = React.createClass({displayName: "exports", 
    render: function() {
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {className: "form-control"}, 
                        React.createElement("option", {"data-categoryid": ""}, this.props.templateText[3].selectCategory), 
                        React.createElement("option", null, this.props.templateText[4].categoryOptions)
                    )
                )
    }
});