var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min');

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            productsList: []
        }
    },
    componentDidMount: function(){
        jQuery(window).scroll(function() {
            if(jQuery(window).scrollTop() + jQuery(window).height() === jQuery(document).height() - 100) {
               console.log("1/4th of bottom!");
           }
        });
        var self = this;
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {
            self.moltin.Product.List({limit:6}, function(products) {
                self.handleProductState(products);
            }, function(error) {
                console.error(error);
            });
        });           
    },
    handleProductState: function(products) {
        this.setState({ productsList: products });
    },
    render: function() {        
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-10 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {templateText: this.props.templateText})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {templateText: this.props.templateText, products: this.state.productsList})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(React.createElement(App, {templateText: templateText}), document.querySelector('.primary-website-section'));