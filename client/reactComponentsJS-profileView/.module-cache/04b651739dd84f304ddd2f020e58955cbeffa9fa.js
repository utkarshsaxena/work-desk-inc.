var React = require('react'),
    ProductList = require('./productList.min');

module.exports = React.createClass({displayName: "exports",
    render: function(){
        console.log(this.props.products);
        /*var productList = this.props.products.map(function(i){
            return <ProductList key={i}/>
        });*/
        return React.createElement("section", {className: "products-section"}, 
                        React.createElement("div", {className: "row"}, 
                            productList
                        )
                    )    
    }
});