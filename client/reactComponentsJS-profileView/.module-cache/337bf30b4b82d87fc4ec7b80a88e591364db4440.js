var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min');

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            productsList: [],
            categoriesList: []
        }
    },
    componentDidMount: function(){        
        var self = this;
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {
            // Get all products
            self.moltin.Product.List(null, function(products) {
                self.setState({ productsList: products });
            }, function(error) {
                console.error(error);
            });
            
            // Get all categories
            self.moltin.Category.List(null, function(categories) {
                self.setState({ categoriesList: categories }); 
                console.log(categories);
            }, function(error) {
                console.error(error);
            });
        });           
    },
    handleProductState: function(products) {
        this.setState({ productsList: products });
    },
    handleCategoriesState: function(categories){
        this.setState({ categoriesList: categories });    
    },
    handleCategoryChange: function(categoryVal){
        console.log('aaa');
        var categoryValue, productList, done=false, self = this;
        if(categoryVal === "All Categories") {
            categoryValue =  null;            
        }else {
            categoryValue = { category: categoryVal  };
        }
        console.log(categoryValue);
        
        function doAsync2() {
            var deferredObject = jQuery.Deferred();
            self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
            self.moltin.Authenticate(function(data,error) {
                // Get all products
                self.moltin.Product.List(categoryValue, function(products) {
                    deferredObject.resolve(products);
                }, function(error) {
                    deferredObject.reject(error);
                });
            });
            return deferredObject.promise();
        }
        
        doAsync2()
            .done(function (products) {
                self.setState({ productsList: products })
        }).fail(function (error) {
            console.log(error);
        });
        
       /*var self = this;
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {
            // Get all products
            self.moltin.Product.List(categoryValue, function(products) {
                done = true; 
                productList = products
                console.log(productList);
            }, function(error) {
                done=false;
                console.error(error);
            });
        });
        
        if (done) this.setState({ productsList: productList });*/
        
        
    },
    render: function() {        
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-10 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {
                    templateText: this.props.templateText, 
                    categories: this.state.categoriesList, 
                    handleCategoryChange: this.handleCategoryChange})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {
                    templateText: this.props.templateText, 
                    products: this.state.productsList})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(React.createElement(App, {templateText: templateText}), document.querySelector('.primary-website-section'));