var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min');

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            productsList: ''
        }
    },
    componentWillMount: function(){
        this.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        console.log(this.moltin);
        this.moltin.Authenticate(function(data,error) { 
                
            console.log(this.moltin);
            console.log(data);
            console.log(this);
        
        }.bind(this));           
    },
    handleProductState: function(products) {
        console.log('handling state');
        this.setState({ productsList: products });
    },
    render: function() {
        
            
        /*this.moltin.Authenticate(function() {
            this.moltin.Product.List(null, function(products) {
                console.log(this);
                console.log(products);
                //productsList.productsData = products;
                //this.handleProductState(products);
            }, function(error) {
                console.error(error);
            });
        });     */   
        
        console.log(this.state.productsList);
        
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-10 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {templateText: this.props.templateText})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {templateText: this.props.templateText, products: this.state.productsList})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(React.createElement(App, {templateText: templateText}), document.querySelector('.primary-website-section'));