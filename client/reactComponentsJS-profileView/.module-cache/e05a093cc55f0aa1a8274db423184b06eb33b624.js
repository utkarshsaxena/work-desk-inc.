var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min');

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            productsList: ''
        }
    },
    componentWillMount: function() {
        var moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        
         moltin.Authenticate(function() {
             
              moltin.Product.List(null, function(products) {
                   //productsList.productsData = products;
                  this.setState({ productsList: products });
                  console.log(this.state.productsList);
              }, function(error) {
                  console.error(error);              
              });
         });
    },
    render: function() {
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-10 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {templateText: this.props.templateText})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {templateText: this.props.templateText, products: this.state.productsList})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(React.createElement(App, {templateText: templateText}), document.querySelector('.primary-website-section'));