var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            hover: false            
        }    
    },
    onMouseEnterHandler: function(){
        this.setState({
            hover: true
        });
    },
    onMouseLeaveHandler: function(){
        this.setState({
            hover: false
        });
    },
    render: function() {
        return React.createElement("div", {className: "col-sm-6 col-md-4"}, 
                
                    React.createElement("div", {className: "thumbnail product-thumbnail", 
                        onMouseEnter: this.onMouseEnterHandler, 
                        onMouseLeave: this.onMouseLeaveHandler}, 
                            React.createElement("div", {className: "hover"}), 
                        React.createElement("div", {className: "thumbnail-footer"}, 
                        React.createElement("p", null, "$", this.props.price)
                    ), 
                      React.createElement("img", {width: "250px", height: "250px", src: this.props.imageURl}), 
                      React.createElement("div", {className: "overlay-text"}, 
                        React.createElement("h3", null, this.props.product), 
                        React.createElement("p", null, "By ", this.props.owner)
                      )
                    )
                  )
    }
});