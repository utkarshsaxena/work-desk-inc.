module.exports = function(categoryValue){
    var moltin, productList, deferredObject = jQuery.Deferred();
    
    moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        moltin.Authenticate(function(data,error) {
            // Get all products
            moltin.Product.List(categoryValue, function(products) {
                productList = products;
                 deferredObject.resolve(products)
            }, function(error) {
                console.error(error);
            });
        });
    return deferredObject;
}