var React = require('react');

module.exports = React.createClass({displayName: "exports",
    handleChange: function(event){
        var options = event.target.options;
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                this.props.handleCategoryInput(options[i].value);
            }
        }
    },
    render: function() {
        
        var options = this.props.categories.map(function(categoryDataProps, i){
            return React.createElement("option", {
            value: categoryDataProps.id, 
            key: i}, 
                categoryDataProps.title
            )
        });
        
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {
                    className: "form-control", 
                    onChange: this.handleChange
                    }, 
                        React.createElement("option", {disable: "disabled"}, this.props.templateText[3].allCategories), 
                    options
                    )
                )
    }
});