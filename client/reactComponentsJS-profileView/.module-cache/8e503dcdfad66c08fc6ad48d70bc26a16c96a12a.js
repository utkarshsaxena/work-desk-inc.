var React = require('react'),
    SearchField = require('./searchField.min'),
    SearchDropDown = require('./searchDropDown.min');

module.exports = React.createClass({displayName: "exports",
    render: function(){
        return React.createElement("section", {className: "search-module well"}, 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-md-12"}, 
                                React.createElement(SearchField, null), 
                                 /* .input-group */
                                 React.createElement("div", {className: "input-group price-dropdown"}, 
                                    React.createElement("div", {className: "input-group-addon"}, "$"), 
                                    React.createElement("select", {className: "form-control"}, 
                                        React.createElement("option", {"data-price-low": "0", "data-price-high": "9999"}, "Select Price"), 
                                        React.createElement("option", {"data-price-low": "0", "data-price-high": "20"}, "0 - 20"), 
                                        React.createElement("option", {"data-price-low": "20", "data-price-high": "40"}, "20 - 40"), 
                                        React.createElement("option", {"data-price-low": "40", "data-price-high": "60"}, "40 - 60"), 
                                        React.createElement("option", {"data-price-low": "60", "data-price-high": "9999"}, "60 +")
                                    )
                                )
                            ), " "/* .col-lg-6 */
                        ), " "/* .row */
                    )
    }
});