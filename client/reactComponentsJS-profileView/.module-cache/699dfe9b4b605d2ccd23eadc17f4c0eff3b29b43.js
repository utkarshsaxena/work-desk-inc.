var React = require('react'),
    SearchField = require('./searchField.min'),
    SearchDropDownCategory = require('./searchDropDownCategory.min'),
    SearchDropDownPrice = require('./searchDropDownPrice.min');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            searchValue: ''            
        }    
    },
    handleSearchInput: function(value){
        this.setState({searchValue: event.target.value});
        console.log(this.state.searchValue); 
    },
    render: function(){
        return React.createElement("section", {className: "search-module well"}, 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-md-12"}, 
                                React.createElement(SearchField, {
                            templateText: this.props.templateText, 
                            handleSearchInput: this.handleSearchInput, 
                            searchValue: this.state.searchValue}), 
                                React.createElement(SearchDropDownCategory, {templateText: this.props.templateText}), 
                                React.createElement("div", {className: "input-group price-dropdown"}, 
                                    React.createElement("div", {className: "input-group-addon"}, this.props.templateText[1].dollar), 
                                    React.createElement(SearchDropDownPrice, {templateText: this.props.templateText})
                                )
                            )
                        ), " "/* .row */
                    )
    }
});