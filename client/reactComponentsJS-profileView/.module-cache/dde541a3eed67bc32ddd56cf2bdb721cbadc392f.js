var React = require('react');


module.exports = React.createClass({displayName: "exports",
    render: function() {
        return React.createElement("div", {className: "col-sm-6 col-md-4"}, 
                    React.createElement("div", {className: "thumbnail product-thumbnail", 
                        onMouseEnter: this.onMouseEnterHandler, 
                        onMouseLeave: this.onMouseLeaveHandler}, 
                        React.createElement("div", {className: "thumbnail-header"}, 
                        React.createElement("p", null, this.props.price.value)
                    ), 
                      React.createElement("img", {src: this.props.images[0].url.http}), 
                      React.createElement("div", {className: "overlay-text"}, 
                        React.createElement("i", {"data-toggle": "tooltip", title: "Add to Cart!", className: "fa fa-cart-plus fa-3x"}), 
                        React.createElement("h3", {className: "overlay-title"}, this.props.title), 
                        React.createElement("p", {className: "overlay-owner"}, "By ", this.props.brand.value)
                      ), 
                        React.createElement("div", {className: "overlay-footer"}, 
                            React.createElement("h4", {
                            className: "overlay-rating"}, 
                            "4/5"
                            )
                        )
                    )
                  )
    }
});