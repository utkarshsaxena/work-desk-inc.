var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min');

var App = React.createClass({displayName: "App",
    render: function() {
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-10 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {templateText: this.props.templateText})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {templateText: this.props.templateText, products: this.props.products})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

var productsList = {};

var moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
     moltin.Authenticate(function() {
         moltin.Product.List(null, function(products) {
             productsList.productsData.push(products);
         }, function(error) {
             console.error(error);
         });
     });


var sampleData = {
    productData: [
        {
            product: 'Matte Blackdsfsdfsdfdsfsdf Sunglasses',
            owner: 'UtBooty',
            imageURl: '/assets/images/product1.jpg',
            price: '160',
            rating: '4.0'
        }]};
    
React.render(React.createElement(App, {templateText: templateText, products: sampleData}), document.querySelector('.primary-website-section'));

