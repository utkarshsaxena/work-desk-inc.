var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            categoryVal: 'All Categories'
        }    
    },
    componentDidMount: function() {
        //this.props.handleCategoryInput(this.state.categoryVal);
    },
    handleChange: function(event){
        var options = event.target.options;
        var valueArray = [];
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                valueArray.push(options[i].value);
            }
        }
        console.log(valueArray);
        this.setState({
            categoryVal: event.target.value
        });
        console.log('apple ' + this.state.categoryVal);
        this.props.handleCategoryInput(this.state.categoryVal);
    },
    render: function() {
        
        var options = this.props.categories.map(function(categoryDataProps, i){
            return React.createElement("option", {
            value: categoryDataProps.id, 
            key: i}, 
                categoryDataProps.title
            )
        });
        
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {
                    className: "form-control", 
                    onChange: this.handleChange
                    }, 
                        React.createElement("option", {disable: "disabled"}, this.props.templateText[3].allCategories), 
                    options
                    )
                )
    }
});