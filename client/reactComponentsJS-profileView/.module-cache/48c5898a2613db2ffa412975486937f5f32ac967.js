var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min'); 

var App = React.createClass({displayName: "App",
    render: function() {
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-sm-4 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {templateText: this.props.templateText})
                ), 
                React.createElement("div", {className: "col-sm-8 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {templateText: this.props.templateText, products: this.props.products})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

var sampleData = {
    productData: [{
        title: 'Show Courses 1',
        number: 12,
        header: 'Learn React',
        description: 'React is a fantastic new front end library for rendering web pages. React is a fantastic new front end library for rendering web pages',
        imageURl: 'https://facebook.github.io/react/img/logo_og.png',
        button1: 'Button',
        button2: 'Button'
    }]
};
    
React.render(React.createElement(App, {templateText: templateText, products: sampleData}), document.querySelector('.primary-website-section'));

