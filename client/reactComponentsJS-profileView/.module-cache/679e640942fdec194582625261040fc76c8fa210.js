var React = require('react'),
    Products = require('./products.min');

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            productsList: [],
            categoriesList: []
        }
    },
    componentDidMount: function(){        
        var filteredList, self = this;
        
        function filterProductList(brand, productList) {
            var i, id,
                l = productList.length,
                filteredProducts = [];

            for (i = 0; i < l; i++) { 
                if(productList[i].brand && productList[i].brand.data && productList[i].brand.data.id === brand){
                    filteredProducts.push(productList[i]);
                }        
            } 
            return filteredProducts;
        }
        
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {
            // Get all products
            self.moltin.Product.List({limit:12}, function(products) {
                //filteredList = filterProductList('123123',products);
                self.setState({ productsList: products });
            }, function(error) {
                console.error(error);
            });
        });           
    },
    handleProductState: function(products) {
        this.setState({ productsList: products });
    },
    render: function() {      
        return React.createElement("div", {className: "col-xs-12 col-sm-6 col-md-7"}, 
                React.createElement(Products, {
                    templateText: this.props.templateText, 
                    products: this.state.productsList})
                )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(React.createElement(App, {templateText: templateText}), document.querySelector('#userProductsProfileView'));