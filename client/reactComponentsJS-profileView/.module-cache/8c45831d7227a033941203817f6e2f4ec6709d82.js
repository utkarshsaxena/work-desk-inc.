var React = require('react');

module.exports = React.createClass({displayName: "exports", 
    render: function() {
        return React.createElement("div", {className: "input-group"}, 
                                    React.createElement("input", {type: "text", className: "form-control", placeholder: "Search for..."}), 
                                    React.createElement("span", {className: "input-group-btn"}, 
                                        React.createElement("button", {className: "btn btn-default", type: "button"}, "Go!")
                                    )
                                )
                                    
                                            
    }
});