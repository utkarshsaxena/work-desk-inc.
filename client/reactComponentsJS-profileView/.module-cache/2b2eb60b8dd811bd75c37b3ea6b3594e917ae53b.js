var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            categoryVal: 'All Categories'
        }    
    },
    handleChange: function(event){
        this.setState({
            categoryVal: event.target.value
        });
    },
    render: function() {
        this.props.handleCategoryInput(this.state.categoryVal);
        var options = this.props.categories.map(function(categoryDataProps, i){
            return React.createElement("option", {
            value: categoryDataProps.id, 
            key: i}, 
                categoryDataProps.title
            )
        });
        
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {
                    className: "form-control", 
                    onChange: this.handleChange, 
                    value: this.state.categoryVal}, 
                        React.createElement("option", null, this.props.templateText[3].allCategories), 
                    options
                    )
                )
    }
});