var React = require('react');

module.exports = React.createClass({displayName: "exports", 
    render: function() {
        return React.createElement("div", {className: "col-sm-6 col-md-4"}, 
                    React.createElement("div", {className: "thumbnail product-thumbnail"}, 
                      React.createElement("img", {width: "400px", height: "400px", src: this.props.imageURl}), 
                      React.createElement("div", {className: "caption"}, 
                        React.createElement("h3", null, this.props.product), 
                        React.createElement("p", null, "By ", this.props.owner)
                      )
                    )
                  )
    }
});