var React = require('react'),
    ProductList = require('./productList.min');

module.exports = React.createClass({displayName: "exports",
    render: function(){
        var productList = this.props.products.map(function(productsProps, i){
            return React.createElement(ProductList, React.__spread({},  productsProps, {key: i}))
        
        });
        return React.createElement("section", {className: "products-section"}, 
                        React.createElement("div", {className: "row"}, 
                            productList
                        )
                    )    
    }
});