var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min'); 

var App = React.createClass({displayName: "App",
    render: function() {
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-6 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {templateText: this.props.templateText})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {templateText: this.props.templateText, products: this.props.products})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

var sampleData = {
    productData: [{
        product: 'Matte Black Sunglasses',
        owner: 'UtBooty',
        imageURl: '/assets/images/productGlasses.jpg'
    },
                  {
        product: 'Matte Black Sunglasses',
        owner: 'UtBooty',
        imageURl: '/assets/images/productGlasses.jpg'
    },
                  {
        product: 'Matte Black Sunglasses',
        owner: 'UtBooty',
        imageURl: '/assets/images/productGlasses.jpg'
    }]
};
    
React.render(React.createElement(App, {templateText: templateText, products: sampleData}), document.querySelector('.primary-website-section'));

