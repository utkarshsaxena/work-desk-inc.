var React = require('react');

module.exports = React.createClass({displayName: "exports",
    getInitialState: function(){
        return {
            searchInputVal: ''            
        }    
    },
    handleChange: function(event){
        console.log(event.target.value);
        this.setState({searchInputVal: event.target.value});
        this.props.searchValue(this.state.searchInputVal);    
    },
    render: function() {
        return React.createElement("div", {className: "input-group"}, 
                    React.createElement("input", {
                    type: "text", 
                    className: "form-control", 
                    placeholder: this.props.templateText[2].searchFor, 
                    onChange: this.handleChange, 
                    value: this.state.searchInputVal}), 
                    React.createElement("span", {className: "input-group-btn"}, 
                        React.createElement("button", {className: "btn btn-default", type: "button"}, this.props.templateText[0].go)
                    )
                )
    }
});