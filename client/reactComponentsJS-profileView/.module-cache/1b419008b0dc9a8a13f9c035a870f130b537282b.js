var React = require('react'),
    SearchField = require('./searchField.min'),
    SearchDropDownCategory = require('./searchDropDownCategory.min'),
    SearchDropDownPrice = require('./searchDropDownPrice.min');

module.exports = React.createClass({displayName: "exports",
    handleSearchInput: function(searchVal){
        var searchValue =  searchVal;
    },
    handlePriceInput: function(priceVal){
        var priceValue =  priceVal;
    },
    handleCategoryInput: function(categoryVal){
        var categoryValue =  categoryVal;
    },
    handleClick: function(){
        // Send all information here and set up the state on success
        console.log('handleClick');
    },
    render: function(){
        return React.createElement("section", {className: "search-module well"}, 
                        React.createElement("div", {className: "row"}, 
                            React.createElement("div", {className: "col-md-12"}, 
                                React.createElement(SearchField, {
                            templateText: this.props.templateText, 
                            handleSearchInput: this.handleSearchInput}), 
                            React.createElement(SearchDropDownCategory, {
                            templateText: this.props.templateText, 
                            handleCategoryInput: this.handleCategoryInput}), 
                                
                                React.createElement("div", {className: "input-group price-dropdown"}, 
                                    React.createElement("div", {className: "input-group-addon"}, this.props.templateText[1].dollar), 
                                    
                                    React.createElement(SearchDropDownPrice, {
                                    handlePriceInput: this.handlePriceInput, 
                                    templateText: this.props.templateText})
                                ), 
                                    
                                React.createElement("button", {
                                className: "btn btn-default", 
                                type: "button", 
                                onClick: this.handleClick}, this.props.templateText[0].search)
                                
                            )
                        ), " "/* .row */
                    )
    }
});