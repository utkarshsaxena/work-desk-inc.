var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min'),
    Q = require('q');

var App = React.createClass({displayName: "App",
    getInitialState: function() {
        return {
            productsList: [],
            categoriesList: []
        }
    }, 
    componentDidMount: function(){        
        this.handleCategoryChange;
    },
    handleProductState: function(products) {
        this.setState({ productsList: products });
    },
    handleCategoriesState: function(categories){
        this.setState({ categoriesList: categories });  
        console.log('aaaapple');
    },
    handleCategoryChange: function(categoryVal){
        console.log('apple');
        var categoryValue, self = this, deferred = Q.defer();
        if(categoryVal === "All Categories") {
            categoryValue =  null;            
        }else {
            categoryValue = { category: categoryVal  };
        }
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {
            deferred.reject(self.moltin);            
        });
        
        Q.when(self.moltin).then(function(moltin) {
            console.log(moltin);        
        })
        
        
    },
    render: function() {        
        return React.createElement("div", {className: "container-fluid"}, 
            React.createElement("div", {className: "row"}, 
                React.createElement("div", {className: "col-xs-10 col-md-3 col-lg-2"}, 
                    React.createElement(SearchForm, {
                    templateText: this.props.templateText, 
                    categories: this.state.categoriesList, 
                    handleCategoryChange: this.handleCategoryChange})
                ), 
                React.createElement("div", {className: "col-xs-12 col-md-9 col-lg-10"}, 
                    React.createElement(Products, {
                    templateText: this.props.templateText, 
                    products: this.state.productsList})
                )
            )
        )
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(React.createElement(App, {templateText: templateText}), document.querySelector('.primary-website-section'));