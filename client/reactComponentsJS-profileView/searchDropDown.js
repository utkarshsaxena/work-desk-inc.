var React = require('react');

module.exports = React.createClass({displayName: "exports", 
    render: function() {
        return React.createElement("div", {className: "dropdown category-dropdown"}, 
                    React.createElement("select", {className: "form-control"}, 
                        React.createElement("option", {"data-categoryid": ""}, "Select a Category"), 
                        React.createElement("option", null, "Apple")
                    )
                )
    }
});