var dashboardConfig = function($interpolateProvider, $routeProvider) {
        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');
        $routeProvider
                .when('/', {
                    templateUrl:'views/dashboardProfileTab.html',
                    controller: 'profileController'
                })
                .when('/ventures', {
                    templateUrl:'views/dashboardProductsTab.html',
                    controller: 'productController'
                })
                .when('/account', {
                    templateUrl:'views/dashboardAccountTab.html',
                    controller: 'accountController'
                })
    }
module.exports = dashboardConfig;