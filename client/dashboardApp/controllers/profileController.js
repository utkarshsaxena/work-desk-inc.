module.exports = function($scope, dashboardService) {
    // Get user information from mongo
    (function(){
        dashboardService.getUserProfileInformation($scope);        
    })();
    
    $scope.userSchools = [
        { id: 1, name: 'The University of British Columbia', shortName: 'UBC' },
        { id: 2, name: 'Simon Fraser University', shortName: 'SFU' },
        { id: 3, name: 'Emily Carr University of Art + Design', shortName: 'ECU' }
    ];   
    
    // Submit profile form data.
    $scope.submitProfileForm = function(isValid) {
        $scope.submitted = true;
        $scope.success = false;
        $scope.error = false;
        
        if (isValid) {
            var formObject = {
                'username' : $scope.username,
                'userEmail': $scope.userEmail,
                'userSchool': {
                    schoolId: $scope.schoolSelected.id,
                    schoolName: $scope.schoolSelected.name,
                    shortName: $scope.schoolSelected.shortName
                },
                'userDescription': $scope.userDescription,
                'fullName': $scope.fbFullName,
                'mongoUserID': $scope.mongoUserID,
                'socialLinks': {
                    linkedin: $scope.userLinkedin || '',
                    behance: $scope.userBehance || '',
                    portfolio: $scope.userPortfolio || '',
                }
            };
            // Get all form values and send to the service
            dashboardService.submitUserProfileInformation($scope, formObject);
            $scope.error = false;
        } else {
            $scope.success = false;
            $scope.errorMessage = 'Woops! Something went wrong';
        }        
    };
}