var moltinController = require('../moltinController.min');

module.exports = function($scope, dashboardService) {
    
    function createSlug (title) {
        var splitArray, sliceArray, cleanTitle;
        cleanTitle = title.replace(/\./g,'').replace(/\,/g,'');
        splitArray = cleanTitle.split(' ');
        sliceArray = splitArray.splice(1, 6);
        sliceArray.push(Math.floor(Math.random() * 10000000));
        return sliceArray.join('-');        
    }    
    
    function matchCategory(categoryName) {
        var i = 0,
            l = $scope.categoryList.length,
            selectedCategory;
        
        for(i=0; i < l; i++) {
            if($scope.categoryList[i].name === categoryName) {
                selectedCategory = $scope.categoryList[i];
            }            
        }
        
        return selectedCategory;
    }
    
    function uploadProductImage($scope, $uploadedFileDetails, productID) {
        // Bad practice to mix angular and jquery
        var form = new FormData;
        form.append('upload', $uploadedFileDetails);
        dashboardService.uploadProductImage($scope, form, productID);
    }
    
    // Get user information from mongo
    (function(){
        dashboardService.getUserProfileInformation($scope);        
    })();
    
    if(!$scope.userBrand) {
        // Create a user brand if it does not exist
        var createBrandInfo = {
            title : $scope.mongoUserID,
            slug : createSlug($scope.fbFullName),
            status: 0,
            description: $scope.fbFullName
        };
        dashboardService.createUserBrand($scope, createBrandInfo)
        .done(function (data) {
            console.log(data);
        }).fail(function (error) {
            console.error(error);
        });
    }
    
    // TODO Replace hardcoded name with dynamic value
    moltinController.getFilteredProducts($scope.userBrand)
        .done(function (product) {
            if(product) {
                $scope.$apply(function() {
                $scope.ventureList = product;
                $scope.selectVentureImageForm = $scope.ventureList[0];
                // Call product change to populate fields with the information of the first product.
              });
            }
        }).fail(function (error) {
            console.error("Failed to get categories.");
            console.error(error);
        });
    
    // Get all categories to show in dropdown.
    moltinController.getCategories()
        .done(function (category) {            
            $scope.$apply(function() {
                $scope.categoryList = category;
                $scope.categorySelected = $scope.categoryList[0];
              });
        }).fail(function (error) {
            console.error("Failed to get categories.");
            console.error(error);
        });
    
    // Initialize drop downs
    $scope.formList = [
        { id: 1, name: 'Create' },
        { id: 2, name: 'Edit' }
    ];
    
    $scope.categoryList = [
        { id: 1, name: 'Loading Categories' }
    ];
    
     
    // Handle Change
    $scope.onFormChange = function(formName){
        // Reset form submitted
        $scope.submitted = false;
        // Populate form data from moltin to the fields.
        // IF formname is create, reset form fields
        // If formname is edit, populate form fields.
        if(formName.name === 'Create') {
            $scope.productTitle = '';
            $scope.productPrice = '';
            $scope.categorySelected = $scope.categoryList[0];
            $scope.stockLevel = '';
            $scope.productDescription = '';
            console.log('create');
        } else if(formName.name === 'Edit'){
            console.log('edit');
            // This else if statement can be removed as the same call is made on page load.
            // Get all user specific products.// TODO Replace hardcoded name with dynamic value
            moltinController.getFilteredProducts($scope.userBrand)
                .done(function (product) {
                    if(product) {
                        $scope.$apply(function() {
                        $scope.ventureList = product;
                        $scope.selectVenture = $scope.ventureList[0];
                        // Call product change to populate fields with the information of the first product.
                        $scope.onProductChange($scope.selectVenture);
                      });
                    }
                }).fail(function (error) {
                    console.error("Failed to get categories.");
                    console.error(error);
                });    
            }
        }
    
    $scope.onProductChange = function(selectedProduct) {
        // Populate product specific data from moltin
        moltinController.getProduct(selectedProduct)
            .done(function (product) {
                if(product) {
                    $scope.$apply(function() {
                    $scope.productTitle = product.title;
                    $scope.productPrice = parseInt(product.price.value.slice(1));
                    $scope.categorySelected = matchCategory(product.category.value);
                    $scope.productDescription = product.description;
                    $scope.stockLevel = product.stock_level;
                  });
                }                
            }).fail(function (error) {
                console.error("Failed to get categories.");
                console.error(error);
            }); 
        
    }
    
    $scope.submitHandleProductForm = function(isValid, formName) {
        $scope.submitted = true;
        $scope.success = false;
        $scope.error = false;
        // Check if edit form or create form
        if(formName.name === "Edit" && isValid) {
            // Entering Edit form
            var editformObject = {
                title : $scope.productTitle,
                slug:  createSlug($scope.productTitle),
                price: $scope.productPrice,
                category: $scope.categorySelected.catID,
                stock_level: $scope.stockLevel,
                description: $scope.productDescription
                
            };
            // Get the form object and call the service
            dashboardService.editProduct($scope, editformObject);
            $scope.error = false;            
        } else if (formName.name === "Create" && isValid) {
            // Entering create form
            // If user brand exists
            var formObject = {
                title : $scope.productTitle,
                slug:  createSlug($scope.productTitle),
                sku: Math.floor(Math.random() * 100000000000).toString(), // random SKU number
                price: $scope.productPrice,
                status: '0', // product not live
                category: $scope.categorySelected.catID,
                stock_level: $scope.stockLevel,
                stock_status: '1', // in stock
                requires_shipping: '0', // No shipping
                catalog_only: '0', // No
                description: $scope.productDescription,
                brand : $scope.userBrand, //replace with dynamic value $scope.userBrand
                tax_band : '1037122560187171424'
            };
            // Get the form object and call the service
            console.log('brand exists');
            dashboardService.createProduct($scope, formObject);
            $scope.error = false;
        } else {
            // If the form is Invalid
                $scope.error = true;
                $scope.errorMessage = 'Woops! Something went wrong';
            }
    }
    
    $scope.handleImageUpload = function() {
        var selectedProductId = $scope.selectVentureImageForm.productID,
            $uploadedFile = jQuery('#uploadProductImage');
        
        if($uploadedFile.val() != '') {
            $scope.imagePresent = true;
            uploadProductImage($scope, $uploadedFile[0].files[0], selectedProductId);
        } else {
            $scope.imagePresent = false; //TODO Form validation.
            $scope.imageFormSuccess = false;
            $scope.imageFormError = true;
            $scope.imageSuccessMessage = '';
            $scope.imageErrorMessage = 'Please upload an image file!'
        }
    }
}