// Handles all non-secure calls to moltin
var moltinController = {},
    moltinReference = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});

// private functions

// Create category results to be used in ng-options
function ngOptionsCategoryObject(moltinData) {
    var i, id,
        l = moltinData.length,
        formattedCategories = [];
    
    for (i = 0; i < l; i++) { 
        var categoryObject = {};
        id = i;
        categoryObject.name = moltinData[i].title;
        categoryObject.description = moltinData[i].description;
        categoryObject.catID = moltinData[i].id;
        categoryObject.id = id + 1;
        formattedCategories.push(categoryObject);
    }
    return formattedCategories;
}

// Create product results to be used in ng-options
function ngOptionsProductObject(filteredProductList) {
    var i, id,
        l = filteredProductList.length,
        formattedProducts = [];
    for (i = 0; i < l; i++) { 
        var categoryObject = {};
        id = i;
        categoryObject.name = filteredProductList[i].title;
        categoryObject.productID = filteredProductList[i].id;
        categoryObject.id = id + 1;
        if(filteredProductList[i].images) {
            categoryObject.imageList = filteredProductList[i].images;
        }
        formattedProducts.push(categoryObject);
    }
    
    return formattedProducts;
}

function filterProductList(brand, productList) {
    var i, id,
        l = productList.length,
        filteredProducts = [];
    
    for (i = 0; i < l; i++) { 
        if(productList[i].brand && productList[i].brand.data && productList[i].brand.data.id === brand){
            filteredProducts.push(productList[i]);
        }        
    } 
    return filteredProducts;
}

// public functions
moltinController.getCategories = function() {
    var moltin, categoryList, formattedList, deferredObject = jQuery.Deferred();
    
    moltinReference.Authenticate(function(data,error) {
        moltinReference.Category.List(null, function(category) {
            formattedList = ngOptionsCategoryObject(category);
            deferredObject.resolve(formattedList);
        }, function(error) {
            console.error(error);
            deferredObject.reject(error);
        });
    });
    return deferredObject;
}

moltinController.getFilteredProducts = function(userBrand) {
    var productList, formattedList, deferredObject = jQuery.Deferred();
    
    moltinReference.Authenticate(function(data,error) {
        moltinReference.Product.List(null, function(products) {
            productList = filterProductList(userBrand, products);
            if(productList) {
                formattedList = ngOptionsProductObject(productList);
                deferredObject.resolve(formattedList);
            } else {
                deferredObject.resolve(false);
            }
        }, function(error) {
            console.error(error);
            deferredObject.reject(error);
        });
    });
    
    return deferredObject;
}

moltinController.getProduct = function(selectedProduct) {
    var productInfo, deferredObject = jQuery.Deferred();
    if(selectedProduct) {
        moltinReference.Authenticate(function(data,error) {
        moltinReference.Product.Get(selectedProduct.productID, function(product) {
                deferredObject.resolve(product);
            }, function(error) {
                deferredObject.reject(error);
            });
        });
    } else {
        deferredObject.resolve(false);
    }
    
    return deferredObject;
}

moltinController.getUserBrand = function(brandId) {
    var brandInfo, deferredObject = jQuery.Deferred();
    
    moltinReference.Authenticate(function(data,error) {
        moltinReference.Brand.Get(brandId, function(brand) {
            deferredObject.resolve(brand);
        }, function(error) {
            // Something went wrong...
            deferredObject.reject(error);
        });
    });
    return deferredObject;    
}

module.exports = moltinController;