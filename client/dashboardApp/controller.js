var profileController = require('./controllers/profileController.min'),
    productController = require('./controllers/productController.min'),
    accountController = require('./controllers/accountController.min'),
    dashboardControllers = [];

dashboardControllers = [profileController, productController, accountController]
module.exports = dashboardControllers;