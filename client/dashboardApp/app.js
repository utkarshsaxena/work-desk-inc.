var angular = require('angular'),
    dashboardControllers = require('./controller.min'),
    dashboardConfig = require('./routes.min'),
    dashboardService = require('./services.min');     

var dashboardApp = angular.module('dashboardApp', [require('angular-route'), require('angular-animate')]);

dashboardApp.controller('profileController', ['$scope', 'dashboardService', dashboardControllers[0]]);

dashboardApp.controller('productController', ['$scope', 'dashboardService', dashboardControllers[1]]);

dashboardApp.controller('accountController', ['$scope', dashboardControllers[2]]);

dashboardApp.config (['$interpolateProvider', '$routeProvider', dashboardConfig]);

dashboardApp.service('dashboardService', ['$http', dashboardService]);

