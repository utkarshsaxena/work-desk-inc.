var moltinController = require('./moltinController.min');

module.exports = function($http) {
    
    // private functions    
    var setCorrectSchoolname = function(schoolArray, selectedSchool){
        var i, l = schoolArray.length;
        for (i=0; i < l; i++) {
            if(schoolArray[i].id === selectedSchool.schoolId) {
                return schoolArray[i];
            }
        }
    }
    
    // public functions
    this.submitUserProfileInformation = function($scope, data) {
        var req = {
            method: 'POST',
            url: '/dashboard',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        }
        $http(req).success(function(data, status, headers, config){
            console.log(data);
            $scope.success = data.success;
            $scope.successMessage = data.message;
        }).error(function(data, status, headers, config){
            console.error(data); 
            $scope.error = true;
            $scope.errorMessage = 'Woops! Something went wrong';
        });
    }
    
    this.saveMoltinBrandInformation = function($scope, data) {
        var req = {
            method: 'POST',
            url: '/dashboard',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        }
        $http(req).success(function(data, status, headers, config){
            console.log(data);
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    }
    
    this.getUserProfileInformation = function($scope, data) {
        var req = {
            method: 'POST',
            url: '/getUserInformation?userid=' + $scope.mongoUserID
        }
        
        $http(req).success(function(data, status, headers, config){
            console.log(data);
            $scope.username = data.userInfo[0].username || '';
            $scope.userEmail = data.userInfo[0].userEmail || '';
            if(data.userInfo[0].userSchool && $scope.userSchools) {
                $scope.schoolSelected  = setCorrectSchoolname($scope.userSchools, data.userInfo[0].userSchool);
            }            
            $scope.userDescription = data.userInfo[0].userDescription || '';
            $scope.userLinkedin = data.userInfo[0].socialLinks.linkedin || '';
            $scope.userBehance = data.userInfo[0].socialLinks.behance || '';
            $scope.userPortfolio = data.userInfo[0].socialLinks.portfolio || '';
        }).error(function(data, status, headers, config){
            console.error(data);
            $scope.error = true;
            $scope.errorMessage = 'Woops! Something went wrong';
        });
    }
    
    this.createProduct = function($scope, formObject){
        var req = {
            method: 'POST',
            url: '/createProduct',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(formObject)
        }
        
        $http(req).success(function(data, status, headers, config){
            data.userInfo = JSON.parse(data.userInfo);
            $scope.success = data.success;
            $scope.successMessage = data.message;
            console.log(data);
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    }
    
    this.createUserBrand = function($scope, brandObject){
        var deferredObject = jQuery.Deferred();
        var self = this;
        var req = {
            method: 'POST',
            url: '/createUserBrand',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(brandObject)
        }
        
        $http(req).success(function(data, status, headers, config){
            data.brandInfo = JSON.parse(data.brandInfo);
            deferredObject.resolve(data);
            $scope.userBrand = data.brandInfo.result.id;
            $scope.userBrandSlug = data.brandInfo.result.slug;
            var onlyBrandFormData = {
                brandInfoToMongo : {
                    brandID : data.brandInfo.result.id,
                    slug : data.brandInfo.result.slug,
                    title : data.brandInfo.result.title,
                    description : data.brandInfo.result.description
                },
                mongoUserID: $scope.mongoUserID
            }  
            // Save result to mongo DB
            self.saveMoltinBrandInformation($scope, onlyBrandFormData);
        }).error(function(data, status, headers, config){
            console.error(data);
            deferredObject.reject(data);
        });
        
        return deferredObject;
    }
    
    this.editProduct = function($scope, editformObject){
        console.log($scope.selectVenture.productID);
        var req = {
            method: 'POST',
            url: '/editProduct?productid=' + $scope.selectVenture.productID,
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(editformObject)
        }
        
        $http(req).success(function(data, status, headers, config){
            data.userInfo = JSON.parse(data.userInfo);
            console.log(data);
        }).error(function(data, status, headers, config){
            console.error(data);
        });
    }
    
    this.uploadProductImage = function($scope, formData, productId) {
        
        jQuery.ajax({
            url: '/uploadProductImage?productid=' + productId,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            type: 'POST',
            success:function (data, status, req) {
                data.imageInfo = JSON.parse(data.imageInfo);
                if(data.imageInfo.status == false) {
                     $scope.$apply(function() {
                         $scope.imageFormSuccess = false;
                         $scope.imageFormError = true;
                         $scope.imageErrorMessage = 'File failed to upload.';
                     });
                } else {
                    $scope.$apply(function() {
                        $scope.imageFormSuccess = true;
                        $scope.imageFormError = false;
                        $scope.imageSuccessMessage = 'Image successfully uploaded!';
                     });
                     moltinController.getFilteredProducts($scope.userBrand)
                        .done(function (product) {
                            if(product) {
                                $scope.$apply(function() {
                                $scope.ventureList = product;
                              });
                            }
                        }).fail(function (error) {
                            console.error("Failed to get categories.");
                            console.error(error);
                        });
                }
                console.log(data);
            },
            error:function (req, status, error) {
                console.log(error);
                $scope.imageFormSuccess = false;
                $scope.imageFormError = true;
                $scope.imageErrorMessage = 'File failed to upload.';
            }
        });
    }
}