var React = require('react');

module.exports = React.createClass({
    getInitialState: function(){
        return {
            priceValue: 'low'            
        }    
    },
    handleChange: function(event){
        this.setState({priceValue: event.target.value});
    },
    render: function() {
        this.props.handlePriceInput(this.state.priceValue);
        return <select
        className="form-control"
        onChange={this.handleChange}
        value={this.state.priceValue}>
        <option value="low">Low to High</option>
        <option value="high">High to Low</option>
                </select>
    } 
});


