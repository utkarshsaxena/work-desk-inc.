var React = require('react'),
    ProductList = require('./productList.min');

module.exports = React.createClass({
    
    render: function(){
        var productList = this.props.products.map(function(productDataProps, i){
            return <ProductList {...productDataProps} key={i}/>
        });
        
        return <section className="products-section">
                        <div className="row">
                            {productList}
                        </div>
                    </section>    
    }
});