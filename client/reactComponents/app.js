var React = require('react'),
    SearchForm = require('./searchForm.min'),
    Products = require('./products.min'),
    MoltinFactory = require('./moltinFactory.min');

var App = React.createClass({
    getInitialState: function() {
        return {
            productsList: [],
            categoriesList: []
        }
    },
    componentDidMount: function(){
        window.addEventListener('scroll', this.handleScroll);
        var self = this;
        self.offset = 0;
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {         
            // Get all products
            self.moltin.Product.List({limit:12}, function(products) {
                self.setState({ productsList: products });
                self.receivedResponse = true;
            }, function(error) {
                console.error(error);
            });
            
            // Get all categories
            self.moltin.Category.List(null, function(categories) {
                self.setState({ categoriesList: categories }); 
            }, function(error) {
                console.error(error);
            });
        });           
    },
    handleProductState: function(products) {
        this.setState({ productsList: products });
    },
    handleCategoriesState: function(categories){
        this.setState({ categoriesList: categories });    
    },
    handleCategoryChange: function(categoryVal){
        var categoryValue, productList, self=this;
        if(categoryVal === "All Categories") {
            categoryValue =  null;            
        } else {
            categoryValue = { category: categoryVal  };
        }
        
        MoltinFactory(categoryValue)
          .done(function (products) {
            productList = products;
            self.setState({ productsList: products });
        }).fail(function () {
            console.error("Failed to update product list");
        });
    },
    handleScroll: function(event) {
         if (((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100)) && this.receivedResponse) {
             var self = this;
             self.offset = self.offset + 12;
             self.receivedResponse = false;
             self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
             self.moltin.Authenticate(function(data,error) {
                 // Get all products
                 self.moltin.Product.List({offset: self.offset, limit:12}, function(products) {
                     if(products[0]) {
                         self.setState({
                             productsList: self.state.productsList.concat(products)
                         });
                         self.receivedResponse = true;
                     } else {
                         self.receivedResponse = false;
                     }                    
                 }, function(error) {
                     console.error(error);
                 });                 
             });
         }
    },
    render: function() {      
        return <div className="container-fluid">
                    <section className="secondary-navbar">
                        <SearchForm
                        templateText={this.props.templateText}
                        categories={this.state.categoriesList}
                        handleCategoryChange={this.handleCategoryChange}/>
                </section>            
                <div className="row">
                    <div className="col-xs-12 col-md-12 col-lg-12">
                        <Products
                        templateText={this.props.templateText}
                        products={this.state.productsList} />
                    </div>
                </div>
            </div>
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(<App templateText={templateText} />, document.querySelector('.primary-website-section'));