var React = require('react');

module.exports = React.createClass({
    handleChange: function(event){
        var options = event.target.options;
        for (var i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                this.props.handleCategoryInput(options[i].value);
            }
        }
    },
    render: function() {
        
        var options = this.props.categories.map(function(categoryDataProps, i){
            return <option
            value={categoryDataProps.id}
            key={i}>
                {categoryDataProps.title}
            </option>
        });
        
        return <div className="dropdown category-dropdown well">
                    <h4>Filter:</h4>
                    <select 
                    className="form-control"
                    onChange={this.handleChange}
                    >
                        <option disable='disabled'>{this.props.templateText[3].allCategories}</option>
                    {options}
                    </select>
                </div>
    }
});