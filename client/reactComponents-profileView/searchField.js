var React = require('react');

module.exports = React.createClass({
    getInitialState: function(){
        return {
            searchValue: ''            
        }    
    },
    handleChange: function(event){
        this.setState({searchValue: event.target.value});
    },
    render: function() {
        this.props.handleSearchInput(this.state.searchValue);
        return <div className="input-group search-input-wrapper">
                    <input
                    type="text"
                    className="form-control"
                    placeholder={this.props.templateText[2].searchFor}
                    onChange={this.handleChange}
                    value={this.state.searchValue}>
                        </input>
                </div>
    }
});