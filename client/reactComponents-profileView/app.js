var React = require('react'),
    Products = require('./products.min');

var App = React.createClass({
    getInitialState: function() {
        return {
            productsList: [],
            categoriesList: []
        }
    },
    componentDidMount: function(){        
        var filteredList,
            self = this,
            brand = jQuery('#userProductsProfileView').attr('data-brandId'); // bad practice??
        
        function filterProductList(brand, productList) {
            var i, id,
                l = productList.length,
                filteredProducts = [];

            for (i = 0; i < l; i++) { 
                if(productList[i].brand && productList[i].brand.value === brand){
                    filteredProducts.push(productList[i]);
                }        
            } 
            return filteredProducts;
        }
        
        self.moltin = new Moltin({publicId:'kUii24GNvLPdWjZfB3lac3vKnLPnLDr6OERRafEZ'});
        self.moltin.Authenticate(function(data,error) {
            // Get all products
            self.moltin.Product.List({limit:12}, function(products) {
                filteredList = filterProductList(brand, products);
                self.setState({ productsList: filteredList });
            }, function(error) {
                console.error(error);
            });
        });           
    },
    handleProductState: function(products) {
        this.setState({ productsList: products });
    },
    render: function() {      
        return <div className="col-xs-12 col-sm-6 col-md-7">
                <Products
                    templateText={this.props.templateText}
                    products={this.state.productsList} />
                </div>
  }
});

var templateText = [
    { search : 'Search'},
    { dollar: '$'},
    { searchFor: 'Search for...'},
    { allCategories: 'All Categories'},
    { categoryOptions: 'apple'},
    { selectPrice: 'Select Price'},
    { selected: 'selected' }
];

React.render(<App templateText={templateText} />, document.querySelector('#userProductsProfileView'));