var React = require('react');

module.exports = React.createClass({
    render: function() {
        return <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div className="thumbnail product-thumbnail"
                        onMouseEnter={this.onMouseEnterHandler}
                        onMouseLeave={this.onMouseLeaveHandler}>
                        <div className="thumbnail-header">
                        <p>{this.props.price.value}</p>
                    </div> 
                      <img height="250px" src={(this.props.images[0]) ? this.props.images[0].url.http : '/assets/images/default_product_image.png' }></img>
                      <div className="overlay-text">
                        <a href={"/product?slug=" + this.props.slug}>
                            <i data-toggle="tooltip" title="View details" className="fa fa-info-circle fa-3x"></i>
                        </a>
                        <h3 className="overlay-title">{this.props.title}</h3>
                        <p className="overlay-owner">By {this.props.brand.data.description}</p>
                      </div>
                        <div className="overlay-footer">
                            <h4
                            className="overlay-rating">
                            4/5
                            </h4>
                        </div>
                    </div>
                  </div>
    }
});